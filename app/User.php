<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use \Storage;

class User extends Authenticatable
{
    use Notifiable;

    const IS_BANNED = 1;
    const IS_ACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public static function add($fields)
    {
        $user = new static;
        $user->fill($fields);
        $user->save();

        return $user;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        if( !empty($fields['password']) )
        {
            $this->password = bcrypt($fields['password']);
        }

        $this->save();
    }

    public function remove()
    {
        $this->removeAvater();
        $this->delete();
    }

    public function uploadAvatar($image)
    {
        if(empty($image)) return;

        $this->removeAvater();

        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads', $filename);
        $this->avatar = $filename;
        $this->save();
    }

    public function removeAvater()
    {
        if(!empty($this->avatar)) {
            Storage::delete('uploads/' . $this->avatar);
        }
    }

    public function get_avatar()
    {
        if(empty($this->avatar)){
            return '/img/no-avatar.jpg';
        }

        return '/uploads/' . $this->avatar;
    }

    public function makeAdmin()
    {
        $this->is_admin = 1;
        $this->save();
    }

    public function makeNormal()
    {
        $this->is_admin = 0;
        $this->save();
    }

    public function toggleAdmin($value)
    {
        if(empty($value)){
            return $this->makeNormal();
        }

        return $this->makeAdmin();
    }

    public function ban() {
        $this->status = User::IS_BANNED;
        $this->save();
    }

    public function unban() {
        $this->status = User::IS_ACTIVE;
        $this->save();
    }

    public function toggleBan($value)
    {
        if(empty($value)){
            return $this->unban();
        }

        return $this->ban();
    }

    public function generate_password($password)
    {
        if(!empty($password)){
            $this->password = bcrypt($password);
            $this->save();
        }
    }
}
