<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
    public function registrationForm()
    {
        return view('pages.register');
    }

    public function registration(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        $user = User::add($request->all());
        $user->generate_password($request->get('password'));

        return redirect('/login');
    }

    public function loginForm()
    {
        return view('pages.login');
    }

    public function login(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $result = Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ]);
        if($result) {
            return redirect('/');
        }
        return redirect()->back()->with('status', 'Incorrect email or password');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
